#include <libnotify/notify.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <sys/inotify.h>

#include <pthread.h>


//windows = 'Documents\\My Games\\StarConflict\\logs'
//linux = '.local/share/starconflict/logs'
//mac = 'Libary/Application Support/Star Conflict/logs'

int notify(char* name, char* msgbody) {
	notify_init ("StarConflict");
	NotifyNotification * msg = notify_notification_new (name, msgbody, "dialog-information");
	notify_notification_show (msg, NULL);
	g_object_unref(G_OBJECT(msg));
	notify_uninit();
	return 0;
}

int parseLine(char* line) {
	if(line[28] == 'P' && line[36] == 'F') {
		char name[32] = "";
		char msg[1024] = "";
		int nameLength = 0;
		int i;

		for(i = 42; line[i] != ']' ;i++) {
			if(isspace(line[i])){
				printf("Spaaace\n");
			}else{
				name[nameLength] = line[i];
				nameLength++;
			}
		}
		strncpy(msg, line + i +2, 1024);
		notify(name, msg);
	}
	return 0;
}

int getNewPath(char* path) {
	struct dirent **namelist;
	int n;
	n = scandir(path, &namelist, NULL, alphasort);
	if (n < 0) {
		perror("scandir");
		return 1;
	} else {
		n--;
		strcat(path, namelist[n]->d_name);
		free(namelist[n]);
		free(namelist);
		return 0;
	}
}

void logThread(char *logsPath) {
	FILE *chatLog;
	char line[1024] = {0};
	printf("%s\n", logsPath);
	do {
		chatLog = fopen(logsPath, "r");
	} while (chatLog == NULL);

	int inotfd = inotify_init();
	if(inotfd < 0) 
		perror( "inotify_init" );

	size_t bufsiz = sizeof(struct inotify_event) + PATH_MAX + 1;
	inotify_add_watch(inotfd, logsPath, IN_MODIFY);
	struct inotify_event* event = malloc(bufsiz);

	while(1) {
		read(inotfd, event, bufsiz);
		while(fgets(line,1024,chatLog) != NULL) {
			parseLine(line);
		}
		clearerr(chatLog);
	}
	fclose(chatLog);
}

int main() {
	char *home = getenv("HOME");
	char *logs = "/.local/share/starconflict/logs/";
	char *logsPath;
	char *path;
	pid_t pid_last;
	if((logsPath = malloc(strlen(home)+strlen(logs)+34)) != NULL){
		logsPath[0] = '\0';
		strcat(logsPath,home);
		strcat(logsPath,logs);
	} else {
		printf("malloc failed!\n");
		return 1;
	}
	path = malloc(strlen(logsPath) + 1);
	path[0] = '\0';
	strcpy(path, logsPath);


	int inotfd = inotify_init();
	if(inotfd < 0)
		perror("inotify_init");

	size_t bufsiz = sizeof(struct inotify_event) + PATH_MAX +1;
	inotify_add_watch(inotfd, path, IN_CREATE);
	struct inotify_event* event = malloc(bufsiz);

	while(1) {
		getNewPath(logsPath);
		strcat(logsPath, "/chat.log");
		pid_last = fork();
		if(pid_last == 0) {
			free(event);
			free(path);
			logThread(logsPath);
			return 1;
		}
		read(inotfd, event, bufsiz);
		strcpy(logsPath, path);
	}

	return 0;
}
