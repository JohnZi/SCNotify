CC ?= gcc
CFLAGS += -Wall $(shell pkg-config --cflags libnotify)
LDFLAGS += $(shell pkg-config --libs libnotify)

PREFIX ?= /usr/local
BINDIR = ${PREFIX}/bin

all: main.c
	${CC} ${CFLAGS} ${LDFLAGS} -o SCNotify main.c

install: SCNotify
	install -D -m 755 SCNotify ${DESTDIR}${BINDIR}/SCNotify

uninstall:
	rm -f ${DESTDIR}${BINDIR}/SCNotify

clean:
	rm SCNotify
